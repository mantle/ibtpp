<?php require("../scripts/template-start.php"); ?>

   <td class="title" valign=top>
		&nbsp;Instructions for Use of BabyBIG<sup>&reg;</sup>
   </td>

</tr>
<tr>
<td class="main">
<span class="emphasis2" style="color:#CC0000">Read all the instructions before BabyBIG<sup>&reg;</sup> arrives at your institution.
Do not reconstitute BabyBIG<sup>&reg;</sup> until instructed by physician.  Infusion must begin within 2 hours of reconstitution.<br />- Please note the current dosage is 50 mg/kg -</span><br><br>

<span class="emphasis1">ATTENTION PHARMACIST:</span>
<br /><br /><b>** Please review and follow all instructions in the FDA-approved package insert **</b><br />
<ol>
<li>BabyBIG<sup>&reg;</sup> should be given as soon after arrival at the pharmacy as can be arranged. <br><br></li>
<li>Please plan in advance of its arrival with the patient's ICU/ward team so as to be ready to promptly rehydrate and administer the medicine when it reaches you.  This means having a secure IV placed and a syringe infusion pump already at the bedside.<br><br></li>
<li>Immediately notify the attending physician/patient care team when BabyBIG<sup>&reg;</sup> arrives.  Confirm with them that they are ready to administer the medicine, then begin its rehydration. Infusion must begin within 2 hours of reconstitution.<br><br></li>
<li>As soon as rehydration is complete (usually about 30 minutes), deliver the medicine to the patient's bedside for administration.<br><br></li>

<li><b>FOLLOWING INFUSION:</b><br>

Please return residual BabyBIG<sup>&reg;</sup> if > 0.5 mL to the pharmacy in the original vial.  Please refrigerate this vial immediately after infusion and return it to our program by overnight courier on cold packs.

<p>
<b>Shipping address:</b><br><br>

California Department of Public Health<br>
Specimen Receiving, Room B106<br>
Attn: IBTPP - Infant Botulism Laboratory<br>
850 Marina Bay Parkway<br>
Richmond, CA 94804-6403<br>
(510) 231-7676
</p>
</li>
</ol>

	</td>

<?php require("../scripts/template-end.php"); ?>
